package com.example.entry.slice;

import com.example.entry.ResourceTable;
import ng.max.slideview.SlideView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        SlideView five = (SlideView) findComponentById(ResourceTable.Id_view_slider_five);
        five.setOnSlideCompleteListener(
                new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        slideView.setEnabled(false);
                        slideView.setText("Disabled");
                    }
                });
        SlideView six = (SlideView) findComponentById(ResourceTable.Id_view_slider_six);
        six.setOnSlideCompleteListener(
                new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        if (slideView.isReversed()){
                            slideView.setReversed(false);
                            slideView.setText("Normal");
                            final int color = getColor(ResourceTable.Color_colorAccent);
                            ((Text) findComponentById(ResourceTable.Id_reverseText)).setTextColor(new Color(color));
                            slideView.setButtonBackgroundColor(color);
                            slideView.setTextColor(color);
                            slideView.setStokeColor(color);
                        }else {
                            slideView.setReversed(true);
                            slideView.setText("Reversed");
                            final int color = getColor(ResourceTable.Color_colorPrimary);
                            ((Text) findComponentById(ResourceTable.Id_reverseText)).setTextColor(new Color(color));
                            slideView.setButtonBackgroundColor(color);
                            slideView.setTextColor(color);
                            slideView.setStokeColor(color);
                        }
                    }
                });
    }

}
