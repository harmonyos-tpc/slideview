# SlideView
A simple, yet awesome sliding button for openharmony.

## Preview
![image1](https://gitee.com/wangjian348/slideview/raw/master/art/slideview_1.PNG)
![image2](https://gitee.com/wangjian348/slideview/raw/master/art/slideview_2.PNG)

## Usage

```
dependencies{
   implementation 'io.openharmony.tpc.thirdlib:slideview:1.0.1'
}
```

The SlideView is very easy to use. Just add it to your layout like any other Component.

##### Via XML

Here's a basic implementation.

```xml
 <ng.max.slideview.SlideView
         ohos:id="$+id:view_slider_one"
         ohos:width="match_parent"
         ohos:height="match_content"
         ohos:top_margin="20vp"

         app:sv_buttonBackgroundColor="#fff"
         app:sv_buttonImage="$media:ic_chevron_double_right_pink"
         app:sv_slideBackgroundColor="$color:colorAccent"
         app:sv_slideText="Accept"
         />

```

Here's an example with all the view attributes.

```xml
<ng.max.slideview.SlideView
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:top_margin="20vp"
        app:sv_slideBackgroundColor="$color:slide_bg_color"
        app:sv_buttonBackgroundColor="$color:button_bg_color"
        app:sv_buttonImage="$media:ic_settings_purple"
        app:sv_slideText="Enabled"
        app:sv_slideTextColor="$color:slide_text_color"
        app:sv_slideTextSize="16fp"
        app:sv_animateSlideText="true"
        app:sv_reverseSlide="true"
        app:sv_strokeColor="$color:slide_stroke_color"
        app:sv_buttonImageDisabled="$media:ic_settings_grey600_24dp"
        />

```

### AttrSet information

|AttrSet name|Description|Default value|
|:-:|:-:|:-:|
|slideBackgroundColor|The slide background color| `#3F51B5` |
|buttonBackgroundColor|The slide button background color| `#FFFFFF`|
|slideTextColor|The color of the slide label |`#FFFFFF`|
|buttonImage|The drawable on the button | double chevron icon |
|slideText|The slide label| `none` |
|slideTextSize|The label's size| 16fp |
|animateSlideText|If `true`, the label fades out while the slide is in progress| `true` |
|strokeColor|If set, a stroke is drawn around the slide background | `none` |
|reverseSlide|If `true`, the SlideView is reversed | `false` |
|buttonImageDisabled| The element to be used as the button image when the SlideView is disabled| the default element|

### Listening for slide actions on the SlideView

You can set a listener to be notified when the user slides across the SlideView. An example is shown below.

```java
SlideView  slideView_one= (SlideView)findComponentById(ResourceTable.Id_slider1);
slideView_one.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                      @Override
                      public void onSlideComplete(SlideView slideView) {

                      }
                  });

```


### Setting the view attrSet via code

For convenience, many of the SlideView attrSet can be set via code.

```java 
 // set the label
 setText(String text);
 
 // set the text size
 setTextSize(int size);
 
 // set the Element for the button
 setButtonImage(Element image);
 
 // set the label color
 setTextColor(int color) ;

 // set the button element for disabled state
 setButtonImageDisabled(Element image);
 
 // set the button background color
 setButtonBackgroundColor(int color);
 
 // set the slide background color
 setSlideBackgroundColor(int color);
 
```
## License
```
Copyright (c) 2017 MAXDeliveryNG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
