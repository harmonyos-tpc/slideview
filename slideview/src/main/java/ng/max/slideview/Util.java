package ng.max.slideview;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

class Util {

    static void setDrawableColor(Element element, int color) {
        if (element instanceof ShapeElement) {
            ShapeElement shapeElement = (ShapeElement) element;
            shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        }
    }

    static void setDrawableStroke(Element element, int color) {
        if (element instanceof ShapeElement) {
            ShapeElement shapeElement = (ShapeElement) element;
            shapeElement.setStroke(4, RgbColor.fromArgbInt(color));
        }
    }
}
